package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main()  {
	text := os.Args[1]
	fmt.Println(text)
	url := "https://quicknotes.spu-labs.dev/api/notes/create"
	fmt.Println("URL:>", url)
	sBody := fmt.Sprintf(`{"msg":"%s"}`, text)
	var jsonStr = []byte(sBody)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	//req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, _ := client.Do(req)
	//if err != nil {
	//	panic(err)
	//}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
}
